package fr.afcepf.ai106.pokemon.dto;

public class ChasseurDTO 
{
	private int id;
	private String nom;	
	private int age;
	
	public ChasseurDTO()
	{
		
	}	
	
	public ChasseurDTO(int id, String nom, int age) {
		super();
		this.id = id;
		this.nom = nom;
		this.age = age;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	
}
