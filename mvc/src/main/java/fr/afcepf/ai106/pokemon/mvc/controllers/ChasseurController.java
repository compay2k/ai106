package fr.afcepf.ai106.pokemon.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afcepf.ai106.pokemon.IChasseurBusiness;
import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

@Controller
public class ChasseurController 
{
	
	@Autowired
	private IChasseurBusiness business;
	
	public void setBusiness(IChasseurBusiness business)
	{
		this.business = business;
	}
	
	@RequestMapping("/chasseur")
	public ModelAndView afficher(
								@RequestParam(value="id", required = false, defaultValue = "1")
								int id)
	{
		ChasseurDTO c = business.getById(id);
		
		ModelAndView mv = new ModelAndView("chasseur");
		mv.addObject("chasseur", c);
		
		return mv;
	}
	
}
