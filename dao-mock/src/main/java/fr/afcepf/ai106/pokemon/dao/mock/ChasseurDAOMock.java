package fr.afcepf.ai106.pokemon.dao.mock;

import java.util.ArrayList;
import java.util.List;

import fr.afcepf.ai106.pokemon.dao.api.IChasseurDAO;
import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

public class ChasseurDAOMock implements IChasseurDAO 
{
	List<ChasseurDTO> chasseurs;
	
	public ChasseurDAOMock()
	{
		chasseurs = new ArrayList<ChasseurDTO>();
		
		chasseurs.add(new ChasseurDTO(1, "Sacha", 12));
		chasseurs.add(new ChasseurDTO(2, "Thierry", 41));
		chasseurs.add(new ChasseurDTO(3, "Bobba Fet", 35));
		chasseurs.add(new ChasseurDTO(4, "Emile", 72));
		chasseurs.add(new ChasseurDTO(5, "Lebon", 50));
	}
	
	
	public List<ChasseurDTO> getAll() 
	{		
		return chasseurs;
	}

	public ChasseurDTO getById(int id) {
		ChasseurDTO result = null;
		
		for (ChasseurDTO c : chasseurs)
		{
			if (c.getId() == id)
			{
				result = c;
				break;
			}
		}
		
		return result;
	}

}
