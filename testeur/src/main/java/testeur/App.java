package testeur;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import fr.afcepf.ai106.pokemon.IChasseurBusiness;

public class App {

	public static void main(String[] args)
	{
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		IChasseurBusiness bu = (IChasseurBusiness)context.getBean("chasseurBU");
		
		System.out.println(bu.getById(2).getNom());
	}
	
}
