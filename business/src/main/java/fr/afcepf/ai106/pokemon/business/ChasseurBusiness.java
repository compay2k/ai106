package fr.afcepf.ai106.pokemon.business;

import java.util.List;

import fr.afcepf.ai106.pokemon.IChasseurBusiness;
import fr.afcepf.ai106.pokemon.dao.api.IChasseurDAO;
import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

public class ChasseurBusiness implements IChasseurBusiness {

	private IChasseurDAO dao;
	
	public ChasseurDTO getById(int id) {
		return dao.getById(id);
	}

	public List<ChasseurDTO> getAll() {
		return dao.getAll();
	}
	
	public void setDao(IChasseurDAO dao)
	{
		this.dao = dao;
	}
}
