package fr.afcepf.ai106.musique;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component(value="sax")
@Primary // sp�cifier que ce bean est le choix par d�faut (si plusieurs trouv�s)
public class Saxophone implements Instrument {

	public void jouer() {
		System.out.println("Pouet Pouet zadd Pouet Pouetqs");

	}

}
