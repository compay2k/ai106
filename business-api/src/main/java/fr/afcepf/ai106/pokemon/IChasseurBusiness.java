package fr.afcepf.ai106.pokemon;

import java.util.List;

import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

public interface IChasseurBusiness 
{
	public ChasseurDTO getById(int id);
	
	public List<ChasseurDTO> getAll();

}
