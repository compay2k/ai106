package fr.afcepf.ai106.pokemon.dao.hibernate.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.BeanUtils;

import fr.afcepf.ai106.pokemon.dao.api.IChasseurDAO;
import fr.afcepf.ai106.pokemon.dao.hibernate.entities.ChasseurEntity;
import fr.afcepf.ai106.pokemon.dto.ChasseurDTO;

public class ChasseurDAOHibernate implements IChasseurDAO {

	@PersistenceContext
	private EntityManager em;
	
	
	public List<ChasseurDTO> getAll() {
		
		List<ChasseurDTO> result = new ArrayList<ChasseurDTO>();
		
		TypedQuery<ChasseurEntity> query = 
				em.createQuery("SELECT c FROM Chasseur c", ChasseurEntity.class);
		
		List<ChasseurEntity> chasseurs = query.getResultList();
		
		for(ChasseurEntity c : chasseurs)
		{
			ChasseurDTO dto = new ChasseurDTO();
			BeanUtils.copyProperties(c, dto);
			result.add(dto);
		}
		
		return result;
	}

	public ChasseurDTO getById(int id) {
		ChasseurDTO result = null;
		
		TypedQuery<ChasseurEntity> query = 
				em.createQuery("SELECT c FROM Chasseur c where c.id = :id", ChasseurEntity.class);
		query.setParameter("id", id);
		
		ChasseurEntity c = query.getSingleResult();
		
		if (c != null)
		{
			result = new ChasseurDTO();
			BeanUtils.copyProperties(c, result);
		}
		
		return result;
	}

	public void setEm(EntityManager em)
	{
		this.em = em;
	}
}
